import token from '../utils/token';
import config from '../config/default.json';

// stubbed data
const expectCartItem = require('./data/expectedResults/cart/singleItemNoDiscount.json');

describe("My Basket", function() {
	this.timeout(5000);
	it("GET products in my basket", async function() {
		const authToken = await login(config.users[0].username, config.users[0].password);
		return request
			.get('/v1.0/cart')
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				expect(res.body.cartPrice).to.deep.equal(expectCartItem.cartPrice);
				expect(res.body.orderLines[0].quantity).to.equal(expectCartItem.orderLines[0].quantity);
				expect(res.body.orderLines[0].sku).to.equal(expectCartItem.orderLines[0].sku);
				expect(res.body.orderLines[0].orderLinePrice).to.deep.equal(expectCartItem.orderLines[0].orderLinePrice);
				expect(res.body.orderLines[0].maxDeliveryDays).to.equal(expectCartItem.orderLines[0].maxDeliveryDays);
			})
	});

	it('GET empty basket', async function() {
		const authToken = await login(config.users[4].username, config.users[4].password);
		return request
			.get('/v1.0/cart')
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				expect(res.body).to.deep.equal({
				  "cartId": "59241498ba1f943dec5789c5",
				  "currency": "GBP",
				  "cartPrice": {
				    "cartOrderLinesTotalWithoutVat": 0,
				    "cartOrderLinesTotalWithVat": 0,
				    "cartGrandTotalWithoutVat": 4.16,
				    "cartGrandTotalWithVat": 4.99,
				    "cartGrandTotalVat": 0.83,
				    "totalShippingPriceWithoutVat": 4.16,
				    "totalShippingPriceWithVat": 4.99,
				    "additionalShippingTotalWithoutVat": 0,
				    "additionalShippingTotalWithVat": 0,
				    "additionalSpendForFreeShipping": 15.01,
				    "beforeDiscountCartOrderLinesTotalWithoutVat": 0,
				    "beforeDiscountCartOrderLinesTotalWithVat": 0,
				    "beforeDiscountCartGrandTotalWithoutVat": 0,
				    "beforeDiscountCartGrandTotalWithVat": 0,
				    "totalDiscountWithoutVat": 0,
				    "totalDiscountWithVat": 0
				  },
				  "orderLines": [],
				  "discount": {
				    "code": null,
				    "warning": null,
				    "termsAndConditions": null,
				    "campaignTitle": ""
				  }
				})
			})
	});
});
