require('babel-core/register');
global.expect = require('chai').expect;
global.nock = require('nock');

const app = require('../../app.js').app;
global.api = app();

global.request = require('supertest-as-promised')(api);
global.login = require('../../utils/login').default;
