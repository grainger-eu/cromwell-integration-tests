// stubbed data for testing output
const expectedSearchResults = require('./data/expectedResults/search/filterByCategoryObject.json');
const expectedProductBySkuSearch = require('./data/expectedResults/search/ProductBySkuSearchObject.json');

// mocked data for usage with nock requests
const nockSearchFilteringByCategoryData = require('./data/nock/search/nockSearchFilteringByCategoryData.json');
const nockProductReturnedBySkuSearchData = require('./data/nock/search/nockProductReturnedBySkuSearchData.json');

// authorization tokens
import token from '../utils/token.js';

// use nock for mocking requests
import config from '../config/default.json';

beforeEach((done) => {
	if (process.env.NOCK) {

		nock(api)
			.post('/v1.0/security/token', { 'sub': 'test' })
			.reply(200, '123')

		nock(api)
			.get('/v1.0/find?skip=0&limit=10&categoryId=39260701')
			.reply(200, nockSearchFilteringByCategoryData);

		nock(api)
			.get('/v1.0/products?sku=KEN2595050K')
			.reply(200, nockProductReturnedBySkuSearchData);

		done();
	} else {
		done();
	}
});

describe("Search", function () {
	it("GET product page with correct attributes after SKU search", async function(done) {
		const authToken = await token();
		request
		.get('/v1.0/products?sku=KEN2595050K')
		.set('Authorization', 'Bearer ' + authToken)
		.end(function (err, res) {
			if (err) return done(err);
			expect(res.statusCode).to.equal(200);
			expect(res.body[0].sku).to.deep.equal(expectedProductBySkuSearch.sku);
			done();
		});
	});

	it("GET category search results with successful response status code", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/find?skip=0&limit=10&categoryId=39260701')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.id).to.deep.equal(expectedSearchResults.id);
				done();
			});
	});
});
