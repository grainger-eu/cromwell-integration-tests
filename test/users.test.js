// authorization tokens
import token from '../utils/token.js';

let timeInMs = Date.now();
let nockedFirstName = '';
let newUser = {};

// stubbed data for testing output
const expectedUsersObject = require('./data/expectedResults/user/userObject.json');
const expectedUserWithNoAddresses = require('./data/expectedResults/user/userObjectNoAddresses.json');
const expectedExistingUserObject = require('./data/expectedResults/user/existingUserObject.json');
const expectedUserAddressList = require('./data/expectedResults/address/userAddressList.json');

// mocked data for usage with nock requests
const nockUserData = require('./data/nock/user/nockUserData.json');
const nockExistingUserData = require('./data/nock/user/nockExistingUserData.json');
const nockExistingUserNoAddress = require('./data/nock/user/nockExistingUserNoAddress.json');
const nockNewUserRegistration = require('./data/nock/user/nockNewUserRegistration.json');
const nockUserAddressList = require('./data/nock/address/nockUserAddressList.json');

beforeEach((done) => {
	if (process.env.NOCK) {

		/* nock requests for users api */

		nock(api)
			.post('/v1.0/security/token', { 'sub': 'test' })
			.reply(200, '123')

		nock(api)
			.post('/v1.0/security/login', {
				'loginData': {
        	'email': 'kamran@10052017-1.com',
        	'password': 'ic3kam123'
      	}
      })
			.reply(200, '321')

		nock(api)
			.post('/v1.0/security/login', {
				'loginData': {
        	'email': 'kamrankhan54@hotmail.com',
        	'password': 'ic3kam123'
      	}
      })
			.reply(200, '321')

		nock(api)
			.get('/v1.0/users/me')
			.reply(200, nockExistingUserData)

		nock(api)
			.post('/v1.0/users', newUser)
			.reply(200, nockNewUserRegistration)

		nock(api)
			.put('/v1.0/users/me/', {
				"email": "kamran@10052017-1.com",
				"firstName": nockedFirstName,
				"lastName": "Khan",
				"title": "Mr"
			})
			.reply(200, {
				"email":"kamrankhan54@hotmail.com",
				"customerId":"Tab5b92e1-9530-4d6e-950c-19f6f91081e6",
				"subId":"",
				"firstName": nockedFirstName,
				"lastName":"Khan",
				"businessDetails":{
					"companyName":null,
					"vatNumber":null,
					"vatRegistred":false
				},
				"defaultShippingAddress":{
					"addressNickName":"Flat 1, Hopps Court 32 Charcot Road",
					"companyName":"",
					"firstName":"Craig",
					"addressLine2":"32 Charcot Road",
					"city":"London",
					"state":"",
					"lastName":"Taub",
					"addressPhoneNumber":"07877647883",
					"addressLine1":"Flat 1, Hopps Court",
					"postalCode":"NW9 5YW",
					"_id":"29360496-ec75-406a-9d99-ec401a92a9a9",
					"isDefaultBillingAddress":true,
					"isDefaultShippingAddress":true},
				"defaultBillingAddress":{
					"addressNickName":"Flat 1, Hopps Court 32 Charcot Road",
					"companyName":"",
					"firstName":"Craig",
					"addressLine2":"32 Charcot Road","city":
					"London","state":"",
					"lastName":"Taub",
					"addressPhoneNumber":"07877647883",
					"addressLine1":"Flat 1, Hopps Court",
					"postalCode":"NW9 5YW",
					"_id":"29360496-ec75-406a-9d99-ec401a92a9a9",
					"isDefaultBillingAddress":true,
					"isDefaultShippingAddress":true},
				"phoneNumbers":{
					"mobile":null,
					"other":null
				},
				"paymentInfo":[
					{
						"cardHolderName":"",
						"cardType":"",
						"cardNumber":""
					}
				],
				"newsletterSubscriber":false,
				"internalStatus":{
					"registrationMessageSent":false
				},
				"title":"Mr",
				"merlinAccountNo":"S10000000"
			})

			/* nock requests for user address api */

			nock(api)
				.get('/v1.0/users/me/addresses')
				.reply(200, nockUserAddressList)

		done();
	} else {
		nockedFirstName = "updated" + timeInMs;
		newUser = {
			"title": 'Mr',
		  "email": 'kamran@' + timeInMs + '.com',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		done();
	}
});

describe("User Login and Registration", function() {
	this.timeout(5000);
	// GET /v1.0/users/me/
	it("GET existing user details", async function(done) {
		const authToken = await login('kamrankhan54@hotmail.com', 'ic3kam123');
		request
			.get('/v1.0/users/me')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body).to.deep.equal(expectedExistingUserObject);
				done();
			});
	});

	it("POST invalid token for login", async function(done) {
		const payload = {
	  	'loginData': {
        'email': 'kamran@30052017-2.com',
        'password': 'ic3kam123'
      }
    }
		request
			.post('/v1.0/security/login')
			.set('Authorization', 'Bearer ' + '123')
			.send(payload)
			.expect(401)
			.then((res) => {
				expect(res.statusCode).to.equal(401);
				expect(res.body.message).to.equal('jwt malformed');
				done();
			})
	});

	it("POST invalid email entered for login", async function(done) {
		const newToken = await token();
	  const payload = {
	  	'loginData': {
        'email': 'skljdslkd',
        'password': 'password'
      }
    }
		request
			.post('/v1.0/security/login')
			.set('Authorization', 'Bearer ' + newToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(401);
				expect(res.body.message).to.be.equal('Invalid email address or password');
				done();
			});
	});

	it("POST valid email entered with no password for login", async function(done) {
		const newToken = await token();
	  const payload = {
	  	'loginData': {
        'email': 'hello@test.com',
        'password': ''
      }
    }
		request
			.post('/v1.0/security/login')
			.set('Authorization', 'Bearer ' + newToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(401);
				expect(res.body.message).to.be.equal('Invalid email address or password');
				done();
			});
	});

	it("POST no email with password for login", async function(done) {
		const newToken = await token();
	  const payload = {
	  	'loginData': {
        'email': '',
        'password': 'password'
      }
    }
		request
			.post('/v1.0/security/login')
			.set('Authorization', 'Bearer ' + newToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(401);
				expect(res.body.message).to.be.equal('Invalid email address or password');
				done();
			});
	});

	it("POST valid email entered with invalid passwords for login and get locked", async function(done) {
		let loginToken = '';
		const newToken = await token();
		const newUser = {
			"title": 'Mr',
		  "email": 'kamran@' + timeInMs + '.com',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + newToken)
			.send(newUser)
			.expect(200)
			.then((res) => {
				loginToken = res.body.token;
				const payload = {
			  	'loginData': {
		        'email': newUser.email,
		        'password': 'thisiswrong'
		      }
		    }
				request
					.post('/v1.0/security/login')
					.set('Authorization', 'Bearer ' + loginToken)
					.send(payload)
					.expect(401)
					.then((res) => {
						expect(res.statusCode).to.equal(401);
						expect(res.body.message).to.equal('Invalid email or password please try again.\n                Attempt 1 of 3');
					})
					.then(() => {
						request
							.post('/v1.0/security/login')
							.set('Authorization', 'Bearer ' + loginToken)
							.send(payload)
							.expect(401)
							.then((res) => {
								expect(res.statusCode).to.equal(401);
								expect(res.body.message).to.equal('Invalid email or password please try again.\n                Attempt 2 of 3');
							})
							.then(() => {
								request
									.post('/v1.0/security/login')
									.set('Authorization', 'Bearer ' + loginToken)
									.send(payload)
									.expect(401)
									.then((res) => {
										expect(res.statusCode).to.equal(401);
										expect(res.body.message).to.equal('Invalid email or password please try again.\n                Attempt 3 of 3');
									})
									.then(() => {
									request
										.post('/v1.0/security/login')
										.set('Authorization', 'Bearer ' + loginToken)
										.send(payload)
										.expect(401)
										.then((res) => {
											expect(res.statusCode).to.equal(401);
											expect(res.body.message).to.equal('Your account has been temporarily suspended. Please try again in 30 mins');
											done();
										});
									});
								});
					});
			});
	});

	// PUT /v1.0/users/me/
	it("PUT existing user details", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const updatedUser = {
			"email": "kamran@10052017-1.com",
			"firstName": nockedFirstName,
			"lastName": "Khan",
			"title": "Mr"
		};
		expectedUserWithNoAddresses.firstName = updatedUser.firstName;
		request
			.put('/v1.0/users/me/')
			.set('Authorization', 'Bearer ' + authToken)
			.send(updatedUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.firstName).to.equal(expectedUserWithNoAddresses.firstName);
				done();
			});
	});

	it("PUT existing user details with invalid email", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const updatedUser = {
			"email": "kamran10052017-1.com",
			"firstName": 'Kamran',
			"lastName": "Khan",
			"title": "Mr"
		};
		request
			.put('/v1.0/users/me/')
			.set('Authorization', 'Bearer ' + authToken)
			.send(updatedUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["email is not a valid email address"]');
				done();
			});
	});

	it("PUT existing user details with no email", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const updatedUser = {
			"email": "",
			"firstName": 'Kamran',
			"lastName": "Khan",
			"title": "Mr"
		};
		request
			.put('/v1.0/users/me/')
			.set('Authorization', 'Bearer ' + authToken)
			.send(updatedUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["email is required","email is not a valid email address"]');
				done();
			});
	});

	it("PUT existing user details with no first name or last name", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const updatedUser = {
			"email": "kamran@10052017-1.com",
			"firstName": '',
			"lastName": '',
			"title": "Mr"
		};
		request
			.put('/v1.0/users/me/')
			.set('Authorization', 'Bearer ' + authToken)
			.send(updatedUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["firstName is required","lastName is required"]');
				done();
			});
	});

	// POST /v1.0/users/me/
	it("POST new user", async function(done) {
		const authToken = await token();
		const newUser = {
			"title": 'Mr',
		  "email": 'kamran@' + timeInMs + timeInMs + '.com',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(newUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(200);
				expect(res.body.token).to.not.be.undefined;
				done();
			});
	});

	it("POST new user with no email address", async function(done) {
		const authToken = await token();
		const invalidUser = {
			"title": 'Mr',
		  "email": '',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(invalidUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(422);
				expect(res.body.message).to.equal('["email is required","email is not a valid email address"]');
				done();
			});
	});

	it("POST new user with invalid email address", async function(done) {
		const authToken = await token();
		const invalidUser = {
			"title": 'Mr',
		  "email": 'hsdjkhskd',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(invalidUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(422);
				expect(res.body.message).to.equal('["email is not a valid email address"]');
				done();
			});
	});

	it("POST new user with existing email address", async function(done) {
		const authToken = await token();
		const invalidUser = {
			"title": 'Mr',
		  "email": 'kamran@10052017-1.com',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(invalidUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(409);
				expect(res.body.message).to.equal('Email already exist. Please login');
				done();
			});
	});

	it("POST new user with no password", async function(done) {
		const authToken = await token();
		const invalidUser = {
			"title": 'Mr',
		  "email": 'kamran@' + timeInMs + '.com',
		  "password": '',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": 'Kamran',
		  "lastName": 'Khan',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(invalidUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(422);
				expect(res.body.message).to.equal('["password is required"]');
				done();
			});
	});

	it("POST new user with no first or last name", async function(done) {
		const authToken = await token();
		const invalidUser = {
			"title": 'Mr',
		  "email": 'kamran@' + timeInMs + '.com',
		  "password": 'password',
		  "customerId": 'cust' + timeInMs,
		  "salt": '123',
		  "firstName": '',
		  "lastName": '',
		  "telephone": '02085982525',
		  "additionalTelephone": '',
		  "companyName": '',
		  "vatNumber": '',
		  "marketPlaceCustomerId": null,
		  "marketPlaceId": null
		};
		request
			.post('/v1.0/users')
			.set('Authorization', 'Bearer ' + authToken)
			.send(invalidUser)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(422);
				expect(res.body.message).to.equal('["firstName is required","lastName is required"]');
				done();
			});
	});

});

describe("User Address Book", function() {
	this.timeout(5000);
	// GET /v1.0/users/me/addresses
	it("GET user address list", async function(done) {
		const authToken = await login('kamrankhan54@hotmail.com', 'ic3kam123');
		request
			.get('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.status).to.equal(200);
				expect(res.body).to.deep.equal(expectedUserAddressList);
				done();
			});
	});

	// PUT /v1.0/users/me/addresses/{id}
	it("PUT user address", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {
			"address": {
				"addressNickName": "29 Gartmore Road",
				"companyName": "",
				"firstName": "Kamran",
				"addressLine2": city,
				"city": "TEST",
				"state": "Essex",
				"lastName": "Khan",
				"addressPhoneNumber": "07511929121",
				"addressLine1": addressLine1,
				"postalCode": "IG3 9XG"
			}
		}

		request
			.put('/v1.0/users/me/addresses/19ca408f-19f9-41d4-be22-b31e97f7e230')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				done();
			});
	});

	// // POST /v1.0/users/me/addresses
	it("POST new user address", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {
			"address": {
				"addressNickName": city,
				"companyName": "",
				"firstName": "Kamran",
				"addressLine2": city,
				"city": "TEST",
				"state": "Essex",
				"lastName": "Khan",
				"addressPhoneNumber": "07511929121",
				"addressLine1": addressLine1,
				"postalCode": "IG3 9XG"
			}
		}

		request
			.post('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.addressLine1).to.equal(payload.address.addressLine1);
				done();
			});
	});

	it("POST new user address with missing address line 1", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {
			"address": {
				"addressNickName": city,
				"companyName": "",
				"firstName": "Kamran",
				"addressLine2": city,
				"city": "TEST",
				"state": "Essex",
				"lastName": "Khan",
				"addressPhoneNumber": "07511929121",
				"addressLine1": "",
				"postalCode": "IG3 9XG"
			}
		}

		request
			.post('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["addressLine1 is required"]');
				done();
			});
	});

	it("POST new user address with empty object", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {}

		request
			.post('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(400);
				expect(res.body[0].messageShort).to.equal('\"address\" is required');
				expect(res.body[0].messageLong).to.equal('There has been a validation error in your request. Please review your input and try again.');
				done();
			});
	});

	it("POST new user address with no first and last name", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {
			"address": {
				"addressNickName": city,
				"companyName": "",
				"firstName": "",
				"addressLine2": city,
				"city": "TEST",
				"state": "Essex",
				"lastName": "",
				"addressPhoneNumber": "07511929121",
				"addressLine1": "29 Gartmore Road",
				"postalCode": "IG3 9XG"
			}
		}

		request
			.post('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["lastName is required","firstName is required"]');
				done();
			});
	});

	it("POST new user address with no postcode", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const city = 'London ' + Date.now();
		const addressLine1 = Date.now() + ' Test Road';
		const payload = {
			"address": {
				"addressNickName": city,
				"companyName": "",
				"firstName": "Kamran",
				"addressLine2": city,
				"city": "TEST",
				"state": "Essex",
				"lastName": "Khan",
				"addressPhoneNumber": "07511929121",
				"addressLine1": "29 Gartmore Road",
				"postalCode": ""
			}
		}

		request
			.post('/v1.0/users/me/addresses')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(422);
				expect(res.body.message).to.equal('["postalCode is required"]');
				done();
			});
	});

});

describe("User Promotion Status", function() {
	// PUT /v1.0/users/me/promotions
	it("PUT user promotion status to true", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const payload = { "subscribed": true };
		request
			.put('/v1.0/users/me/promotions')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.newsletterSubscriber).to.equal(true);
				done();
			});
	});

	it("PUT user promotion status to false", async function(done) {
		const authToken = await login('kamran@10052017-1.com', 'ic3kam123');
		const payload = { "subscribed": false };
		request
			.put('/v1.0/users/me/promotions')
			.set('Authorization', 'Bearer ' + authToken)
			.send(payload)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.newsletterSubscriber).to.equal(false);
				done();
			});
	});
});
