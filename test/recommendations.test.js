import config from '../config/default.json';

const expectRecommendation = require('./data/expectedResults/recommendation/recommendations.json');

describe("Recommendations", function() {
  this.timeout(5000);
  it('GET recommendations for a product',  async function() {
    const authToken = await login(config.users[0].username, config.users[0].password);
    const sku = 'YRK2457780K';
    return request
      .get(`/v1.0/recommendation/product?sku=${sku}`)
      .set('Authorization', 'Bearer ' + authToken)
      .expect(200)
      .then((res) => {
        const firstRecommendation = res.body[0];

        expect(res.body).lengthOf(20);
        expect(firstRecommendation.stock).to.eql(expectRecommendation.stock);
        expect(firstRecommendation.family).to.eql(expectRecommendation.family);
        expect(firstRecommendation.name).to.eql(expectRecommendation.name);
        expect(firstRecommendation.brand).to.eql(expectRecommendation.brand);
      })
  });
});
