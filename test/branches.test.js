import token from '../utils/token';

describe('Branch Locator', function () {
	this.timeout(5000);
	it('GET branches by full postcode', async () => {
		const authToken = await token();
		return request
			.get('/v1.0/branches?postcode=e14%206hj%2C%20UK&distance=25')
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				expect(res.body).to.deep.equal([{
						"_id": "58f8ad6a78a1ba637bc5ff16",
						"branchId": 9,
						"organisationId": 1,
						"branchName": "London",
						"address": {
							"branchAddress1": "Unit 21-22, Muirhead Quay",
							"branchAddress2": "Quay Road",
							"branchAddress3": "Barking",
							"branchAddress4": "London",
							"branchPostcode": "IG11 7BG",
							"branchCountry": "UK",
							"branchCountryCode": "GBR"
						},
						"branchDefaultCurrency": "00",
						"contacts": {
							"branchSalesTelephone": "020 8477 4120",
							"branchAccountsTelephone": "01162 888000",
							"branchSalesFax": "020 8477 4121",
							"branchSalesEmail": "london@cromwell.co.uk",
							"branchAccountsEmail": "london@cromwell.co.uk",
							"branchNotificationEmail": "weborderslondon@cromwell.co.uk"
						},
						"company": {
							"branchCompanyRegNo": "Registered in England No: 986161",
							"branchVatRegNo": "VAT Reg No: GB 115 5713 87",
							"branchTaxLocationId": 1,
							"depot": "0039",
							"id": 39
						},
						"map": {
							"branchMapPdf": "london.pdf",
							"branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=51.530673,0.072816&sll=51.530673,0.072816&ie=UTF8&z=14"
						},
						"branchOpeningTimes": "Mon-Thurs 7:45am - 5:00pm\r\nFriday 7:45am - 4:00pm\r\nSaturday 7:30am - 11:00am",
						"branchMapImage": "london.png",
						"branchRegionId": 170,
						"branchMapCoordinatesLat": 51.530673,
						"branchMapCoordinatesLong": 0.072816,
						"primaryStockLocation": "039",
						"branchSEOName": "london",
						"importSystemId": 2,
						"distance": 4.306463073678853
					},
					{
						"_id": "58f8ad6a78a1ba637bc5ff08",
						"branchId": 45,
						"organisationId": 1,
						"branchName": "Basildon",
						"address": {
							"branchAddress1": "Unit 22 Heronsgate Ind. Est",
							"branchAddress2": "",
							"branchAddress3": "Paycocke Road",
							"branchAddress4": "Basildon",
							"branchPostcode": "SS14 3EU",
							"branchCountry": "UK",
							"branchCountryCode": "GBR"
						},
						"branchDefaultCurrency": "00",
						"contacts": {
							"branchSalesTelephone": "01268 532555",
							"branchAccountsTelephone": "01162 888000",
							"branchSalesFax": "01268 289714",
							"branchSalesEmail": "basildon@cromwell.co.uk",
							"branchAccountsEmail": "basildon@cromwell.co.uk",
							"branchNotificationEmail": "webordersbasildon@cromwell.co.uk"
						},
						"company": {
							"branchCompanyRegNo": "Registered in England No: 986161",
							"branchVatRegNo": "VAT Reg No: GB 115 5713 87",
							"branchTaxLocationId": 1,
							"depot": "0014",
							"id": 14
						},
						"map": {
							"branchMapPdf": "basildon.pdf",
							"branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=51.589669,0.494256&sll=51.589669,0.494256&ie=UTF8&z=14"
						},
						"branchOpeningTimes": "Mon-Thurs 8:00am - 5:15pm\r\nFriday 8:00am - 4:00pm\r\nSaturday 8:00am - 11:30am",
						"branchMapImage": "basildon.png",
						"branchRegionId": 160,
						"branchMapCoordinatesLat": 51.589669,
						"branchMapCoordinatesLong": 0.494256,
						"primaryStockLocation": "014",
						"branchSEOName": "basildon",
						"importSystemId": 2,
						"distance": 22.853449724533654
					},
					{
						"_id": "58f8ad6a78a1ba637bc5ff0f",
						"branchId": 46,
						"organisationId": 1,
						"branchName": "Rochester",
						"address": {
							"branchAddress1": "Unit 2-3 Gills Court",
							"branchAddress2": "Chaucer Close",
							"branchAddress3": "Medway City Estate",
							"branchAddress4": "Rochester, Kent",
							"branchPostcode": "ME2 4NR",
							"branchCountry": "UK",
							"branchCountryCode": "GBR"
						},
						"branchDefaultCurrency": "00",
						"contacts": {
							"branchSalesTelephone": "01634 290586",
							"branchAccountsTelephone": "01162 888000",
							"branchSalesFax": "01634 290589",
							"branchSalesEmail": "rochester@cromwell.co.uk",
							"branchAccountsEmail": "rochester@cromwell.co.uk",
							"branchNotificationEmail": "webordersrochester@cromwell.co.uk"
						},
						"company": {
							"branchCompanyRegNo": "Registered in England No: 986161",
							"branchVatRegNo": "VAT Reg No: GB 115 5713 87",
							"branchTaxLocationId": 1,
							"depot": "0021",
							"id": 21
						},
						"map": {
							"branchMapPdf": "rochester.pdf",
							"branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=51.397545,0.515461&sll=51.397545,0.515461&ie=UTF8&z=14"
						},
						"branchOpeningTimes": "Mon-Thurs 8:00am - 5:00pm\r\nFriday 8:00am - 4:00pm\r\nSaturday 8.00am - 11:00am",
						"branchMapImage": "rochester.png",
						"branchRegionId": 170,
						"branchMapCoordinatesLat": 51.397545,
						"branchMapCoordinatesLong": 0.515461,
						"primaryStockLocation": "021",
						"branchSEOName": "rochester",
						"importSystemId": 2,
						"distance": 24.492663149923175
					}
				]);
			});
		});
		
		it('GET branches by partial postcode', async () => {
			const authToken = await token();
			return request
				.get('/v1.0/branches?postcode=l8%2C%20UK&distance=25')
				.set('Authorization', 'Bearer ' + authToken)
				.expect(200)
				.then((res) => {
					expect(res.body).to.deep.equal([
					  {
					    "_id": "58f8ad6a78a1ba637bc5ff24",
					    "branchId": 99,
					    "organisationId": 1,
					    "branchName": "Liverpool",
					    "address": {
					      "branchAddress1": "Unit A2",
					      "branchAddress2": "Kingfisher Business Park",
					      "branchAddress3": "Hawthorne Road",
					      "branchAddress4": "Bootle, Liverpool",
					      "branchPostcode": "L20 6PF",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "0151 933 2348",
					      "branchAccountsTelephone": "0151 933 2348",
					      "branchSalesFax": "0151 933 7026",
					      "branchSalesEmail": "liverpool@cromwell.co.uk",
					      "branchAccountsEmail": "liverpool@cromwell.co.uk",
					      "branchNotificationEmail": "webordersliverpool@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0210",
					      "id": 210
					    },
					    "map": {
					      "branchMapPdf": "liverpool.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.465384,-2.993303&sll=53.465384,-2.993303&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 7.30am - 5:00pm\r\nFriday 7:30am - 4:00pm\r\nSaturday 8:30am - 12:00am",
					    "branchMapImage": "liverpool.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.465384,
					    "branchMapCoordinatesLong": -2.993303,
					    "primaryStockLocation": "210",
					    "branchSEOName": "liverpool",
					    "importSystemId": 2,
					    "distance": 5.452566013719602
					  },
					  {
					    "_id": "58f8ad6a78a1ba637bc5ff14",
					    "branchId": 14,
					    "organisationId": 1,
					    "branchName": "Chester",
					    "address": {
					      "branchAddress1": "29-31 Alvis Road",
					      "branchAddress2": "Engineer Park",
					      "branchAddress3": "Sandycroft",
					      "branchAddress4": "Deeside, Clywd",
					      "branchPostcode": "CH5 2QB",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "01244 530400",
					      "branchAccountsTelephone": "01162 888000",
					      "branchSalesFax": "01244 532100",
					      "branchSalesEmail": "chester@cromwell.co.uk",
					      "branchAccountsEmail": "chester@cromwell.co.uk",
					      "branchNotificationEmail": "weborderschester@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0037",
					      "id": 37
					    },
					    "map": {
					      "branchMapPdf": "chester.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.199606,-2.995528&sll=53.199606,-2.995528&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 07.45am - 5:00pm\r\nFriday 07.45am - 4:00pm\r\nSaturday 8:00am - 11:30am",
					    "branchMapImage": "chester.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.19992,
					    "branchMapCoordinatesLong": -2.995315,
					    "primaryStockLocation": "037",
					    "branchSEOName": "chester",
					    "importSystemId": 2,
					    "distance": 13.156864269600847
					  },
					  {
					    "_id": "58f8ad6a78a1ba637bc5ff1a",
					    "branchId": 35,
					    "organisationId": 1,
					    "branchName": "Warrington",
					    "address": {
					      "branchAddress1": "Unit 26, Gateway 49 Trade Park",
					      "branchAddress2": "Kerfoot Street",
					      "branchAddress3": "Warrington",
					      "branchAddress4": "Cheshire",
					      "branchPostcode": "WA2 8NT",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "01925 635 466",
					      "branchAccountsTelephone": "01162 888000",
					      "branchSalesFax": "01925 650 091",
					      "branchSalesEmail": "warrington@cromwell.co.uk",
					      "branchAccountsEmail": "warrington@cromwell.co.uk",
					      "branchNotificationEmail": "weborderswarrington@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0059",
					      "id": 59
					    },
					    "map": {
					      "branchMapPdf": "warrington.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.400533,-2.596828&sll=53.400533,-2.596828&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 7:30am - 5:15pm\r\nFriday 7:30am - 4:00pm\r\nSaturday 9:00am - 12:30pm",
					    "branchMapImage": "warrington.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.400533,
					    "branchMapCoordinatesLong": -2.596828,
					    "primaryStockLocation": "059",
					    "branchSEOName": "warrington",
					    "importSystemId": 2,
					    "distance": 14.957282302308391
					  }
					]);
				});
		});

		it('GET branches by location', async () => {
			const authToken = await token();
			return request
				.get('/v1.0/branches?postcode=manchester%2C%20UK&distance=25')
				.set('Authorization', 'Bearer ' + authToken)
				.expect(200)
				.then((res) => {
					expect(res.body).to.deep.equal([
					  {
					    "_id": "58f8ad6a78a1ba637bc5fef9",
					    "branchId": 25,
					    "organisationId": 1,
					    "branchName": "Manchester",
					    "address": {
					      "branchAddress1": "651 Eccles New Road",
					      "branchAddress2": "",
					      "branchAddress3": "Salford",
					      "branchAddress4": "Lancashire",
					      "branchPostcode": "M50 1BA",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "0161 786 2500",
					      "branchAccountsTelephone": "01162 888000",
					      "branchSalesFax": "0161 787 7113",
					      "branchSalesEmail": "manchester@cromwell.co.uk",
					      "branchAccountsEmail": "manchester@cromwell.co.uk",
					      "branchNotificationEmail": "webordersmanchester@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0025",
					      "id": 25
					    },
					    "map": {
					      "branchMapPdf": "manchester.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.483851,-2.319305&sll=53.483851,-2.319305&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 7:30am - 5:15pm\r\nFriday 7:30am - 4:00pm\r\nSaturday 8:00am - 11:30am",
					    "branchMapImage": "manchester.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.483851,
					    "branchMapCoordinatesLong": -2.319305,
					    "primaryStockLocation": "025",
					    "branchSEOName": "manchester",
					    "importSystemId": 2,
					    "distance": 3.159929181695878
					  },
					  {
					    "_id": "58f8ad6a78a1ba637bc5ff1a",
					    "branchId": 35,
					    "organisationId": 1,
					    "branchName": "Warrington",
					    "address": {
					      "branchAddress1": "Unit 26, Gateway 49 Trade Park",
					      "branchAddress2": "Kerfoot Street",
					      "branchAddress3": "Warrington",
					      "branchAddress4": "Cheshire",
					      "branchPostcode": "WA2 8NT",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "01925 635 466",
					      "branchAccountsTelephone": "01162 888000",
					      "branchSalesFax": "01925 650 091",
					      "branchSalesEmail": "warrington@cromwell.co.uk",
					      "branchAccountsEmail": "warrington@cromwell.co.uk",
					      "branchNotificationEmail": "weborderswarrington@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0059",
					      "id": 59
					    },
					    "map": {
					      "branchMapPdf": "warrington.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.400533,-2.596828&sll=53.400533,-2.596828&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 7:30am - 5:15pm\r\nFriday 7:30am - 4:00pm\r\nSaturday 9:00am - 12:30pm",
					    "branchMapImage": "warrington.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.400533,
					    "branchMapCoordinatesLong": -2.596828,
					    "primaryStockLocation": "059",
					    "branchSEOName": "warrington",
					    "importSystemId": 2,
					    "distance": 15.596551787448329
					  },
					  {
					    "_id": "58f8ad6a78a1ba637bc5ff17",
					    "branchId": 8,
					    "organisationId": 1,
					    "branchName": "Blackburn",
					    "address": {
					      "branchAddress1": "Unit 5 Centurion Park",
					      "branchAddress2": "Davyfield Road",
					      "branchAddress3": "",
					      "branchAddress4": "Blackburn",
					      "branchPostcode": "BB1 2QY",
					      "branchCountry": "UK",
					      "branchCountryCode": "GBR"
					    },
					    "branchDefaultCurrency": "00",
					    "contacts": {
					      "branchSalesTelephone": "01254 291 580",
					      "branchAccountsTelephone": "01162 888000",
					      "branchSalesFax": "01254 291 585",
					      "branchSalesEmail": "blackburn@cromwell.co.uk",
					      "branchAccountsEmail": "blackburn@cromwell.co.uk",
					      "branchNotificationEmail": "webordersblackburn@cromwell.co.uk"
					    },
					    "company": {
					      "branchCompanyRegNo": "Registered in England No: 986161",
					      "branchVatRegNo": "VAT Reg No: GB 115 5713 87",
					      "branchTaxLocationId": 1,
					      "depot": "0040",
					      "id": 40
					    },
					    "map": {
					      "branchMapPdf": "blackburn.pdf",
					      "branchMapLink": "http://maps.google.co.uk/maps?f=d&hl=en&geocode=&daddr=53.756831,-2.518272&sll=53.756831,-2.518272&ie=UTF8&z=14"
					    },
					    "branchOpeningTimes": "Mon-Thurs 7:30am - 5:15pm\r\nFriday 7:30am - 4:00pm\r\nSaturday 8:00am - 11:30am",
					    "branchMapImage": "blackburn.png",
					    "branchRegionId": 120,
					    "branchMapCoordinatesLat": 53.716526,
					    "branchMapCoordinatesLong": -2.461137,
					    "primaryStockLocation": "040",
					    "branchSEOName": "blackburn",
					    "importSystemId": 2,
					    "distance": 18.592270323276416
					  }
					]);
				});
		});

		it('GET branches by partial postcode and search within the UK (2000 mile radius)', async () => {
			const authToken = await token();
			return request
				.get('/v1.0/branches?postcode=e14%2C%20UK&distance=2000')
				.set('Authorization', 'Bearer ' + authToken)
				.then((res) => {
					expect(res.body).to.deep.equal(UKSearch);
				});
		});

		it('GET branches not returned when invalid location entered', async () => {
			const authToken = await token();
			return request
				.get('/v1.0/branches?postcode=sdkjshds%2C%20UK&distance=25')
				.set('Authorization', 'Bearer ' + authToken)
				.expect(400)
				.then((res) => {
					expect(res.body).to.deep.equal({
					  "statusCode": 400,
					  "error": "Bad Request",
					  "message": "Cannot get data for provided postcode"
					});
				});
		});
});
