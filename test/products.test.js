// authorization tokens
import token from '../utils/token.js';

describe("Active Products", function() {

	// GET /v1.0/products/{id}
	it("GET active product pack when no stock with lead time", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/QFT6455476X')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when in stock with lead time", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/QFT6040242V')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when in stock with no lead time", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/SHR0250005G')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when not in stock with lead time", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/MTL6629516Y')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when not in stock with lead time", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/MTL6629516Y')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when stock in other suppliers but not in Cromwell", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/QUE8040140K')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET active product when product is stopped and stock is available", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/SHR0725062E')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(true);
				done();
			});
	});

	it("GET inactive product when product is deleted", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/QFT6001422G')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(false);
				done();
			});
	});

	it("GET inactive product when product is stopped and no stock", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/SSF9614010A')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(false);
				done();
			});
	});

	it("GET inactive product when product is quarantined", async function(done) {
		const authToken = await token();
		request
			.get('/v1.0/products/VER3007404E')
			.set('Authorization', 'Bearer ' + authToken)
			.end(function (err, res) {
				if (err) return done(err);
				expect(res.statusCode).to.equal(200);
				expect(res.body.isSearchable).to.equal(false);
				done();
			});
	});
});
