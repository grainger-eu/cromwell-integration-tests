import token from '../utils/token';

const expectedAddress = {
				"found": "1",
				"credits_display_text": "Universal Full PAF 8,705 Credits",
				"accountadminpage": "https://www.simplylookupadmin.co.uk/WebAccountLogin.aspx?doid=1&coid=333332A633&Pay=yes",
				"errormessage": "",
				"id": "16873854_0S_F",
				"organisation": "",
				"line1": "Flat 10",
				"line2": "Russell House",
				"line3": "Saracen Street",
				"town": "London",
				"county": "",
				"postcode": "E14 6HJ",
				"country": "England",
				"rawpostcode": "E146HJ",
				"deliverypointsuffix": "1B",
				"nohouseholds": "1",
				"smallorg": "N",
				"pobox": "",
				"mailsortcode": "61132",
				"udprn": "8068598"
			};

const expectedAddresses = {
	"found": "1",
	"credits_display_text": "Universal Full PAF 8,675 Credits",
	"accountadminpage": "https://www.simplylookupadmin.co.uk/WebAccountLogin.aspx?doid=1&coid=333332A633&Pay=yes",
	"errormessage": "",
	"maxresults": "0",
	"recordcount": "30",
	"records": [{
			"l": "Flat 1 Russell House  Saracen Street London E14 6HJ",
			"id": "16873853_0S_F"
		},
		{
			"l": "Flat 10 Russell House  Saracen Street London E14 6HJ",
			"id": "16873854_0S_F"
		},
		{
			"l": "Flat 11 Russell House  Saracen Street London E14 6HJ",
			"id": "16873855_0S_F"
		},
		{
			"l": "Flat 12 Russell House  Saracen Street London E14 6HJ",
			"id": "16873856_0S_F"
		},
		{
			"l": "Flat 13 Russell House  Saracen Street London E14 6HJ",
			"id": "16873857_0S_F"
		},
		{
			"l": "Flat 14 Russell House  Saracen Street London E14 6HJ",
			"id": "16873858_0S_F"
		},
		{
			"l": "Flat 15 Russell House  Saracen Street London E14 6HJ",
			"id": "16873859_0S_F"
		},
		{
			"l": "Flat 16 Russell House  Saracen Street London E14 6HJ",
			"id": "16873860_0S_F"
		},
		{
			"l": "Flat 17 Russell House  Saracen Street London E14 6HJ",
			"id": "16873861_0S_F"
		},
		{
			"l": "Flat 18 Russell House  Saracen Street London E14 6HJ",
			"id": "16873862_0S_F"
		},
		{
			"l": "Flat 19 Russell House  Saracen Street London E14 6HJ",
			"id": "16873863_0S_F"
		},
		{
			"l": "Flat 2 Russell House  Saracen Street London E14 6HJ",
			"id": "16873864_0S_F"
		},
		{
			"l": "Flat 20 Russell House  Saracen Street London E14 6HJ",
			"id": "16873865_0S_F"
		},
		{
			"l": "Flat 21 Russell House  Saracen Street London E14 6HJ",
			"id": "16873866_0S_F"
		},
		{
			"l": "Flat 22 Russell House  Saracen Street London E14 6HJ",
			"id": "16873867_0S_F"
		},
		{
			"l": "Flat 23 Russell House  Saracen Street London E14 6HJ",
			"id": "16873868_0S_F"
		},
		{
			"l": "Flat 24 Russell House  Saracen Street London E14 6HJ",
			"id": "16873869_0S_F"
		},
		{
			"l": "Flat 25 Russell House  Saracen Street London E14 6HJ",
			"id": "16873870_0S_F"
		},
		{
			"l": "Flat 26 Russell House  Saracen Street London E14 6HJ",
			"id": "16873871_0S_F"
		},
		{
			"l": "Flat 27 Russell House  Saracen Street London E14 6HJ",
			"id": "16873872_0S_F"
		},
		{
			"l": "Flat 28 Russell House  Saracen Street London E14 6HJ",
			"id": "16873873_0S_F"
		},
		{
			"l": "Flat 29 Russell House  Saracen Street London E14 6HJ",
			"id": "16873874_0S_F"
		},
		{
			"l": "Flat 3 Russell House  Saracen Street London E14 6HJ",
			"id": "16873875_0S_F"
		},
		{
			"l": "Flat 30 Russell House  Saracen Street London E14 6HJ",
			"id": "16873876_0S_F"
		},
		{
			"l": "Flat 4 Russell House  Saracen Street London E14 6HJ",
			"id": "16873877_0S_F"
		},
		{
			"l": "Flat 5 Russell House  Saracen Street London E14 6HJ",
			"id": "16873878_0S_F"
		},
		{
			"l": "Flat 6 Russell House  Saracen Street London E14 6HJ",
			"id": "16873879_0S_F"
		},
		{
			"l": "Flat 7 Russell House  Saracen Street London E14 6HJ",
			"id": "16873880_0S_F"
		},
		{
			"l": "Flat 8 Russell House  Saracen Street London E14 6HJ",
			"id": "16873881_0S_F"
		},
		{
			"l": "Flat 9 Russell House  Saracen Street London E14 6HJ",
			"id": "16873882_0S_F"
		}
	]
};

// Skipping these tests as it will eat up our postcode finder allowance
describe("Find Address", function() {
	this.timeout(5000);
	it.skip('GET address by id', async function() {
		const authToken = await token();
		return request
			.get('/v1.0/addresses/16873854_0S_F')
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				expect(res.body).to.deep.equal(expectedAddress);
			});
	});

	it.skip('GET address by postcode', async function() {
		const authToken = await token();
		return request
			.get('/v1.0/addresses/postcode-finder?postcode=e14%206hj')
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				expect(res.body).to.deep.equal(expectedAddresses);
			});
	});
});
