import token from '../utils/token';
import config from '../config/default.json';

describe("Placing Orders", function() {
	this.timeout(5000);
	it("POST my single order without discount", async function() {
		const authToken = await login(config.users[0].username, config.users[0].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWithoutDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderLines[0].orderLinePrice).to.deep.equal(expectedOrderLinesForSingleOrderWithoutDiscount);
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWithoutDiscount);
			});
	});

	it("POST my single order with fixed discount", async function() {
		const authToken = await login(config.users[1].username, config.users[1].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWithFixedDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderLines[0].orderLinePrice).to.deep.equal(expectedOrderLinesForSingleOrderWithFixedDiscount);
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWithFixedDiscount);
			});
	});

	it("POST my single order with percentage discount", async function() {
		const authToken = await login(config.users[5].username, config.users[5].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWithPercentageDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderLines[0].orderLinePrice).to.deep.equal(expectedOrderLineForSingleOrderWithPercentageDiscount);
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWithPercentageDiscount);
			});
	});

	it("POST my multiple line order without discount", async function() {
		const authToken = await login(config.users[6].username, config.users[6].password);
		return request
			.post('/v1.0/orders')
			.send(multipleLineOrderWithoutDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				res.body.orderLines.forEach((actualOrderLines, key) => {
					expect(actualOrderLines.orderLinePrice).to.deep.equal(expectedOrderLinesForMultipleOrderWithoutDiscount[key]);
				});
			});
	});

	it("POST my multiple line order with fixed discount", async function() {
		const authToken = await login(config.users[2].username, config.users[2].password);
		return request
			.post('/v1.0/orders')
			.send(multipleLineOrderWithFixedDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				res.body.orderLines.forEach((actualOrderLines, key) => {
					expect(actualOrderLines.orderLinePrice).to.deep.equal(expectedOrderLinesForMultipleOrderWithFixedDiscount[key]);
				})
			});
	});

	it("POST my multiple line order with different quantities and fixed discount", async function() {
		const authToken = await login(config.users[3].username, config.users[3].password);
		return request
			.post('/v1.0/orders')
			.send(multipleLineOrderWithQuantitiesAndFixedDiscountPayload)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				res.body.orderLines.forEach((actualOrderLines, key) => {
					expect(actualOrderLines.orderLinePrice).to.deep.equal(expectedOrderLinesForMultipleOrderWithQuantitiyAndFixedDiscount[key]);
				})
			});
	});

	it("POST my single order with 3 FOR 2 offer", async function() {
		const authToken = await login(config.users[7].username, config.users[7].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWith3for2discount)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWith3FOR2Discount);
			});
	});

	it("POST my single order with FREE product offer", async function() {
		const authToken = await login(config.users[8].username, config.users[8].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWithFreeProductOffer)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWithFreeProductOffer);
			});
	});

	it("POST my single order with BUY 1 GET 1 FREE offer", async function() {
		const authToken = await login(config.users[9].username, config.users[9].password);
		return request
			.post('/v1.0/orders')
			.send(singleOrderWithBuy1Get1FreeOffer)
			.set('Authorization', 'Bearer ' + authToken)
			.expect(200)
			.then((res) => {
				return request
					.get('/v1.0/orders/' + res.body.orderId)
					.set('Authorization', 'Bearer ' + authToken)
					.expect(200)
			})
			.then((res) => {
				expect(res.body.orderPrice).to.deep.equal(expectedOrderPriceForSingleOrderWithBuy1Get1FreeOffer);
			});
	});

});
