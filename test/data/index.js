// stubbed data for order tests
global.singleOrderWithoutDiscountPayload = require('./nock/orders/singleOrderWithoutDiscountPayload.json');
global.singleOrderWithFixedDiscountPayload = require('./nock/orders/singleOrderWithFixedDiscountPayload.json');
global.singleOrderWithPercentageDiscountPayload = require('./nock/orders/singleOrderWithPercentageDiscountPayload.json');
global.multipleLineOrderWithoutDiscountPayload = require('./nock/orders/multipleLineOrderWithoutDiscountPayload.json');
global.multipleLineOrderWithFixedDiscountPayload = require('./nock/orders/multipleLineOrderWithFixedDiscountPayload.json');
global.multipleLineOrderWithQuantitiesAndFixedDiscountPayload = require('./nock/orders/multipleLineOrderWithQuantitiesAndFixedDiscountPayload.json');
global.singleOrderWithPercentageDiscountPayload = require('./nock/orders/singleOrderWithPercentageDiscountPayload.json');
global.multipleLineOrderWithoutDiscountPayload = require('./nock/orders/multipleLineOrderWithoutDiscountPayload.json');
global.singleOrderWith3for2discount = require('./nock/orders/singleOrderWith3for2discount.json');
global.singleOrderWithFreeProductOffer = require('./nock/orders/singleOrderWithFreeProductOffer.json');
global.singleOrderWithBuy1Get1FreeOffer = require('./nock/orders/singleOrderWithBuy1Get1FreeOffer.json');

// Branch locator
global.UKSearch = require('./expectedResults/branches/UKSearch.json');

// expected order results for single order without discount
global.expectedOrderLinesForSingleOrderWithoutDiscount = require('./expectedResults/orders/expectedOrderLinesForSingleOrderWithoutDiscount.json');
global.expectedOrderPriceForSingleOrderWithoutDiscount = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWithoutDiscount.json');

// expect order results for single order with fixed discount
global.expectedOrderLinesForSingleOrderWithFixedDiscount = require('./expectedResults/orders/expectedOrderLinesForSingleOrderWithFixedDiscount.json');
global.expectedOrderPriceForSingleOrderWithFixedDiscount = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWithFixedDiscount.json');

// expect order results for multiple line order with fixed discount of £40 off total order
global.expectedOrderLinesForMultipleOrderWithFixedDiscount = require('./expectedResults/orders/expectedOrderLinesForMultipleOrderWithFixedDiscount.json');

// expect order results for multiple line order with different quantities and fixed discount of £40 off total order
global.expectedOrderLinesForMultipleOrderWithQuantitiyAndFixedDiscount = require('./expectedResults/orders/expectedOrderLinesForMultipleOrderWithQuantitiyAndFixedDiscount.json');

// expected order line result for single order with percentage discount
global.expectedOrderLineForSingleOrderWithPercentageDiscount = require('./expectedResults/orders/expectedOrderLineForSingleOrderWithPercentageDiscount.json');

// expected order price for single order with percentage discount
global.expectedOrderPriceForSingleOrderWithPercentageDiscount = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWithPercentageDiscount.json');

// expected multiple line order without discount
global.expectedOrderLinesForMultipleOrderWithoutDiscount = require('./expectedResults/orders/expectedOrderLinesForMultipleOrderWithoutDiscount.json');

// expected order price for multiple order without discount
global.expectedOrderPriceForMultipleOrderWithoutDiscount = require('./expectedResults/orders/expectedOrderPriceForMultipleOrderWithoutDiscount.json');

// expected order price for single order with 3 for 2 discount offer
global.expectedOrderPriceForSingleOrderWith3FOR2Discount = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWith3FOR2Discount.json');

// expected order price for single order with FREE product offer
global.expectedOrderPriceForSingleOrderWithFreeProductOffer = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWithFreeProductOffer.json');

// expected order price for single order with BUY 1 GET 1 FREE offer
global.expectedOrderPriceForSingleOrderWithBuy1Get1FreeOffer = require('./expectedResults/orders/expectedOrderPriceForSingleOrderWithBuy1Get1FreeOffer.json');
