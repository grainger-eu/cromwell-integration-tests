const app = () => {
	switch (process.env.TEST_ENV) {
		case 'local': return 'http://localhost:3000';
		case 'development': return 'https://dev-restapi.cromsun.co.uk';
		case 'dev1': return 'https://restapi-dev1.cromsun.co.uk';
		case 'dev2': return 'https://restapi-dev2.cromsun.co.uk';
		case 'dev3': return 'https://restapi-dev3.cromsun.co.uk';
		case 'production': '';
		default: return console.log('No environment provided in TEST_ENV');
	}
}

export { app };
