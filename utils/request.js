import axios from 'axios';

function log(message) {
  if (!process.env.SILENT) {
    console.log(message);
  }
}

 /**
   * Issues a http request
   * @param {{method: string, url: string, body: object, headers: object}} request
   * @return {promise} Request response
   */
const request = async (args) => {
  log('Api Request: "' + JSON.stringify(args) + '"');
  const options = {
    method: args.method,
    url: args.url
  };

  if (args.body !== 'undefined') {
    options.data = args.body;
  }
  if (args.headers !== 'undefined') {
    options.headers = args.headers;
  }
  return axios(options)
  .then((response) => {
    log('Api Response: "' + JSON.stringify(response.data) + '"');
    return response;
  })
  .catch((error) => {
    log('Api Error: "' + error.message + '"');
    return error;
  });
};

export { request };
