import { request } from './request';
import token from './token.js';
import { app } from '../app.js';
const api = app();

export default async function (email, password) {
  const newToken = await token();
  const tokenGenObj = {
    method: 'post',
    url: api + '/v1.0/security/login',
    body: {
      'loginData': {
        'email': email,
        'password': password
      }
    },
    headers: { 'Authorization': 'Bearer ' + newToken }
  };

  const result = await request(tokenGenObj);
  const auth = result.data.token;
  return auth;
}
