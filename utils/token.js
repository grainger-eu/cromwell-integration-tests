import { request } from './request';
import { app } from '../app.js';
const api = app();

export default async function () {
  const tokenGenObj = {
    method: 'post',
    url: api + '/v1.0/security/token',
    body: {
      'sub': 'test'
    }
  };

  const result = await request(tokenGenObj);
  const token = result.data.token;
  return token;
}
