# CROMWELL INTEGRATION TESTS #

Testing Cromwell's API.

### Environment variables
 - `TEST_ENV` - required for every test.
 - `SILENT` - if true will not output API request and response results.

### Running Tests Locally ###

1. Start api-services locally and make sure local mongo also running
2. In command line, run
```
#!javascript

TEST_ENV=local npm test
```

### Running Tests on Development with Mocked Requests ###
1. In command line, run
```
#!javascript

TEST_ENV=development npm test
```

This uses nock.js to mock server requests

### Running Tests on Development without mocking requests ###
1. In command line, run
```
#!javascript

npm test
```

This will make real requests to development
